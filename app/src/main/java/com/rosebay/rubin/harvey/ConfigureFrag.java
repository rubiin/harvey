package com.rosebay.rubin.harvey;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.appyvet.materialrangebar.RangeBar;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;



/// api key          AIzaSyAayeIzUyTxKUyjzPxXtFTRO2NQZWdwtIQ

/**
 * Created by rubin on 1/3/2018.
 */
public class ConfigureFrag extends Fragment implements OnMapReadyCallback,RangeBar.OnRangeBarChangeListener{

    static View rootView;
    SupportMapFragment mapFrag;
    private GoogleMap mMap;
    RangeBar atemp,ahum,shum;
    EditText atemplower,atemphigher,ahumlower,ahumhigher,shumhigher,shumlower;
    SharedPreferences pref;
    String prefsName="settings";
    SharedPreferences.Editor editor;

    public ConfigureFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment





        mapFrag=(SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.mapcon);
        if(mapFrag==null){

            FragmentManager fm= getFragmentManager();
            FragmentTransaction ft =fm.beginTransaction();
            mapFrag=SupportMapFragment.newInstance();
            ft.replace(R.id.mapcon,mapFrag).commit();

        }

        mapFrag.getMapAsync(this);

        if(rootView==null){
            rootView = inflater.inflate(R.layout.configure, container, false);
        }

        atemp=(RangeBar)rootView.findViewById(R.id.atemp);
        ahum=(RangeBar)rootView.findViewById(R.id.ahum);
        shum=(RangeBar)rootView.findViewById(R.id.shum);
        ahumlower=(EditText)rootView.findViewById(R.id.ahumlower);
        ahumhigher=(EditText)rootView.findViewById(R.id.ahumhigher);

        atemphigher=(EditText)rootView.findViewById(R.id.atemphigher);
        atemplower=(EditText)rootView.findViewById(R.id.atemplower);

        shumhigher=(EditText)rootView.findViewById(R.id.shumhigher);
        shumlower=(EditText)rootView.findViewById(R.id.shumlower);

        ahum.setOnRangeBarChangeListener(this);
        atemp.setOnRangeBarChangeListener(this);
        shum.setOnRangeBarChangeListener(this);

        return rootView;
    }
    public void toaster(EditText a1,EditText b1,int a,int b){
      //  Toast.makeText(getActivity(), "Lower: "+a+"Upper: "+b, Toast.LENGTH_SHORT).show();
     a1.setText(a+"");
        b1.setText(b+"");
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.animateCamera( CameraUpdateFactory.zoomTo( 17.0f ) );
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,15));

      //  mMap.animateCamera(CameraUpdateFactory.zoomIn());
    }



    public void LoadValues(){

        pref= this.getActivity().getSharedPreferences(prefsName, Context.MODE_PRIVATE);

        atemplower.setText(pref.getString("atemplower",null));
        atemphigher.setText(pref.getString("atemphigher",null));
        ahumlower.setText(pref.getString("ahumlower",null));
        ahumhigher.setText(pref.getString("ahumhigher",null));
        shumlower.setText(pref.getString("shumlower",null));
        shumhigher.setText(pref.getString("shumhigher",null));
        atemp.setRangePinsByIndices(Integer.parseInt(pref.getString("atemplower",null)),Integer.parseInt(pref.getString("atemphigher",null)));
        ahum.setRangePinsByIndices(Integer.parseInt(pref.getString("ahumlower",null)),Integer.parseInt(pref.getString("ahumphigher",null)));
        shum.setRangePinsByIndices(Integer.parseInt(pref.getString("shumlower",null)),Integer.parseInt(pref.getString("shumhigher",null)));

       editor= pref.edit();

    }

    @Override
    public void onRangeChangeListener(RangeBar rangeBar, int leftPinIndex, int rightPinIndex, String leftPinValue, String rightPinValue) {
        switch (rangeBar.getId()) {
            case  R.id.ahum: {
                // do something for button 1 click
                toaster(ahumlower,ahumhigher,leftPinIndex,rightPinIndex);
            //  editor.putString("ahumlower",leftPinIndex+"");
              //  editor.putString("ahumhigher",rightPinIndex+"");
            //    editor.commit();
                break;
            }

            case R.id.atemp: {
                // do something for button 2 click
                toaster(atemplower,atemphigher,leftPinIndex,rightPinIndex);
                //    editor.putString("atemplower",leftPinIndex+"");
                //      editor.putString("atemphigher",rightPinIndex+"");
                //     editor.commit();
                break;
            }
            case R.id.shum: {
                // do something for button 2 click
                toaster(shumlower,shumhigher,leftPinIndex,rightPinIndex);
                //      editor.putString("shumlower",leftPinIndex+"");
                //      editor.putString("shumhigher",rightPinIndex+"");
                //       editor.commit();
                break;
            }
            //.... etc
        }
    }
}
