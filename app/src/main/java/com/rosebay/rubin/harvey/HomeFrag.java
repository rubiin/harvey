package com.rosebay.rubin.harvey;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;



/// api key          AIzaSyAayeIzUyTxKUyjzPxXtFTRO2NQZWdwtIQ

/**
 * Created by rubin on 1/3/2018.
 */
public class HomeFrag extends Fragment implements OnMapReadyCallback {

    static View rootView;
    SupportMapFragment mapFrag;
    private GoogleMap mMap;

    public HomeFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment



mapFrag=(SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);
        if(mapFrag==null){

            FragmentManager fm= getFragmentManager();
            FragmentTransaction ft =fm.beginTransaction();
            mapFrag=SupportMapFragment.newInstance();
            ft.replace(R.id.map,mapFrag).commit();

        }

        mapFrag.getMapAsync(this);

        if(rootView==null){
            rootView = inflater.inflate(R.layout.home, container, false);
        }
        return rootView;
    }
    private void moveToCurrentLocation(LatLng currentLocation)
    {




    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.animateCamera( CameraUpdateFactory.zoomTo( 17.0f ) );
        mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        LatLng sydney2 = new LatLng(-49, 30);
        LatLng sydney3= new LatLng(0, 30);
        LatLng sydney4 = new LatLng(-90, 90);
        LatLng sydney5= new LatLng(9, 56);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker1"));
        mMap.addMarker(new MarkerOptions().position(sydney2).title("Marker2"));
;       mMap.addMarker(new MarkerOptions().position(sydney3).title("Marker3"));
        mMap.addMarker(new MarkerOptions().position(sydney4).title("Marker4"));
        mMap.addMarker(new MarkerOptions().position(sydney5).title("Marker5"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,15));

    }
}
